from django.urls import path, include
from .views import story8

app_name="story8"

urlpatterns = [
    path('', story8, name="story8"),
]
