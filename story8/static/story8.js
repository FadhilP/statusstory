$(document).ready(() => {
    $('#results')[0].innerHTML = "<h3>Patience</h3>";
    
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=piano',
        success: function(books){
            console.log(books);
            $('#results').empty();
            $('#results').append(    
                "<tr>" +
                    "<th>Title</th>" +
                    "<th>Book Cover</th>" +
                    "<th>Author(s)</th>" +
                    "<th>Details</th>" +
                "</tr>"
            );
            for(i=0 ; i < books.items.length; i++){
                $(`#results`).append(`<tr id=row${i}></tr>`);
                $(`#row${i}`).append("<td><p>" + books.items[i].volumeInfo.title + "</p></td>");                    
                try{    
                $(`#row${i}`).append("<td><img src='" + books.items[i].volumeInfo.imageLinks.thumbnail + "'></img></td>");
                }
                catch{
                    $(`#row${i}`).append("<td><p> No image </p></td>");                   
                }
                try{
                    if(books.items[i].volumeInfo.authors.length > 0){
                        $(`#row${i}`).append(`
                        <td>
                            <ul>
                                ${books.items[i].volumeInfo.authors.map((obj) => {
                                    return `<li>${obj}</li>`
                                }).join("")}
                            </ul>
                        </td>
                        `)
                    }
                    else{
                        $(`#row${i}`).append("<td><p> Unknown authors </p></td>");
                    }
                }
                catch{
                    $(`#row${i}`).append("<td><p> Unknown authors </p></td>");  
                }
                try{
                    $(`#row${i}`).append("<td><a href='" + books.items[i].volumeInfo.infoLink + "'>Details</a></td>");  
                }
                catch{
                    $(`#row${i}`).append("<td><p> No details </p></td>");  
                }
            }

                }
            })

    $('#button').click(function(){
        let key = $("#search").val();
        $('#results')[0].innerHTML = "<h3>Searching "+key+"</h3>"
        console.log('aaaaaa');
        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(books){
                // console.log(books.items[0].searchInfo);
                console.log(books.items)
                $('#results').empty();
                $('#results').append(    
                    "<tr>" +
                        "<th>Title</th>" +
                        "<th>Book Cover</th>" +
                        "<th>Author(s)</th>" +
                        "<th>Details</th>" +
                    "</tr>"
                );
                for(i=0 ; i < books.items.length; i++){
                    $(`#results`).append(`<tr id=row${i}></tr>`);
                    $(`#row${i}`).append("<td><p>" + books.items[i].volumeInfo.title + "</p></td>");                    
                    try{    
                    $(`#row${i}`).append("<td><img src='" + books.items[i].volumeInfo.imageLinks.thumbnail + "'></img></td>");
                    }
                    catch{
                        $(`#row${i}`).append("<td><p> No image </p></td>");                   
                    }
                    try{
                        if(books.items[i].volumeInfo.authors.length > 0){
                            $(`#row${i}`).append(`
                            <td>
                                <ul>
                                    ${books.items[i].volumeInfo.authors.map((obj) => {
                                        return `<li>${obj}</li>`
                                    }).join("")}
                                </ul>
                            </td>
                            `)
                        }
                        else{
                            $(`#row${i}`).append("<td><p> Unknown authors </p></td>");
                        }
                    }
                    catch{
                        $(`#row${i}`).append("<td><p> Unknown authors </p></td>");  
                    }
                    try{
                        $(`#row${i}`).append("<td><a href='" + books.items[i].volumeInfo.infoLink + "'>Details</a></td>");  
                    }
                    catch{
                        $(`#row${i}`).append("<td><p> No details </p></td>");  
                    }
                }
            }
        })
    })
})
