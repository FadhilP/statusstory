from django.test import TestCase, Client

# Create your tests here.
class test(TestCase):
    def test_html_url(self):
        response = self.client.get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')
