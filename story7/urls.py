from django.urls import path, include
from .views import profile

app_name="story7"

urlpatterns = [
    path('', profile, name="profile"),
]
