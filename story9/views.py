from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def story9(request):
    return render(request, "home.html")

def login1(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/story9')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def logout1(request):
    logout(request)
    return redirect('/story9/login')
