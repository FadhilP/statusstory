from django.urls import path, include
from .views import *

app_name="story9"

urlpatterns = [
    path('', story9, name="story9"),
    path('login/', login1, name="login"),
    path('logout/', logout1, name="logout")
]
