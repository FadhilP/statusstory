from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import story9, login1, logout1

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class TestStory7(TestCase):
    def test__html(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_view(self):
        handler = resolve('/story9/')
        self.assertEqual(handler.func, story9)

    def test_login(self):
        handler = resolve('/story9/login/')
        self.assertEqual(handler.func, login1)
        
    def test_logout(self):
        handler = resolve('/story9/logout/')
        self.assertEqual(handler.func, logout1)

