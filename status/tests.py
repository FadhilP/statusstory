from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from django.urls import reverse, resolve
import time, datetime, pytz

from .models import Status
from .views import status

class Test(StaticLiveServerTestCase):

    #unit test
    def test_model(self):
        temp = Status.objects.create(status="halo")
        self.assertEqual(str(temp), "halo")

    def test_views(self):
        self.assertEqual(resolve('/').func, status)

    def test_status_page(self):
        self.assertEqual(self.client.get('/').status_code, 200)

    #functional test
    def setUp(self):
        self.client = Client()

        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="status/chrome_driver_77/chromedriver.exe")
    
    def tearDown(self):
        self.browser.close()

    def test_title(self):

        # self.browser.get('http://localhost:8000')

        # self.browser.get('http://fadhilstatus.herokuapp.com')
        self.browser.get(self.live_server_url)
        self.assertEquals(self.browser.title, "Status")

    def test_opening_text(self):

        # self.browser.get('http://localhost:8000')

        # self.browser.get('http://fadhilstatus.herokuapp.com')
        self.browser.get(self.live_server_url)
        
        text = self.browser.find_element_by_class_name('halo').text
        self.assertEquals(text, 'Halo, apa kabar?')
    
    def test_make_status(self):

        # self.browser.get('http://localhost:8000')

        # self.browser.get('http://fadhilstatus.herokuapp.com')
        self.browser.get(self.live_server_url)

        status_form = self.browser.find_element_by_id("input-status")
        submit = self.browser.find_element_by_id("submit")
        status_form.send_keys("halo")
        submit.click()
        self.assertIn( "halo", self.browser.page_source)

    def test_date(self):

        # self.browser.get('http://localhost:8000')

        # self.browser.get('http://fadhilstatus.herokuapp.com')
        self.browser.get(self.live_server_url)

        status_form = self.browser.find_element_by_id("input-status")
        submit = self.browser.find_element_by_id("submit")
        status_form.send_keys("halo")
        submit.click()

        self.assertIn(datetime.datetime.now().strftime("%d %b %Y | %H:%M"), self.browser.page_source)

    





    