from django.urls import path, include
from .views import status, load_balancer

app_name="status"

urlpatterns = [
    path('', status, name="status"),
    path('load', load_balancer, name="load")
]
