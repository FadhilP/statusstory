from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ["status"]
    
    def __init__(self, *args, **kwargs):
        super(StatusForm, self).__init__(*args, **kwargs)

        self.fields["status"].widget.attrs['class'] = "input-status"
        self.fields["status"].widget.attrs['id'] = "input-status"
